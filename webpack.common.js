const path = require('path');

module.exports = {
  entry: {
    main: './src/limpet.js',
    test: './src/run-tests.js',
  },
  output: {
    filename: 'limpet-[name].js',
    library: "limpet",
    path: path.resolve(__dirname, 'dist'),
  }
};
