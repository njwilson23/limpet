# limpet

*limpet* is a simple functional programming language with procedures,
lexographic scope, and a scheme-like syntax.

```
(def (fac n)
  (if (= (log n) 0)
    1
    (* n (fac (- n 1)))))

(fac 21)
```

## Status

- [x] tokenizer
- [x] expression evaluation
- [x] basic types
- [x] arithmetic operations
- [x] lexical scoping
- [x] defines
- [x] procs (abstraction)
- [x] if expressions (branching)
- [x] do expressions (sequencing)
- [x] comments
- [x] tail call optimization
- [x] variable length argument lists
- [ ] cond expressions
- [ ] lambdas
- [ ] modules
- [ ] simple macros

## Development

To build:

    npm run build

or

    npm run build-prod

After building, run tests with:

    npm run test

## Language reference

### Expressions: atoms and forms

Limpet expressions consists of atomics and forms. Atomics are scalar values,
while forms are abstractions for composing atomics.

Atoms may be booleans, numbers, or strings.

All forms return a value. There are three categories of forms:

1. Procs, which are either user-defined or language-provided and can be applied with a set of bound values. Most of these are defined in limpet, while a kernel have Javascript definitions.
2. Vectors, which are ordered finite collections of expressions.
3. Special forms, which are language-provided and implement functionality that wouldn't be expressible with only Procs.

Special forms include:

- `def name expr`, which binds an expression to a name to the currently-scoped environment and returns the expression
- `def (name [arg1 [arg2 [arg3...]]]) expr`, which binds a Proc to a name in the currently-scoped environment and returns the Proc
- `if expr expr expr`, which lazily evaluates one of two expressions based the value of another
- `do expr expr expr`, which evaluates a series of expressions within a new scope and returns the value of the final expression

### Environments

Environments contain a set of bindings between names and expressions. An
environment may contain a pointer to another environment, forming a chain along
which limpet searches for a name.

It is illegal to redefine a binding _within_ an environment, but a binding in a
higher environment may be shadowed. That is,

```
(def foo "first declaration")

(def foo "second declaration")
```
is an error, but
```
(def foo "first declaration")

(do
  (def foo "second declaration")
  (log foo)) ; logs "second declaration"

(log foo) ; logs "first declaration"
```
is not.

### Syntax

Names are alphanumeric, and may contain inner hyphens or forward-slashes, as
well as terminal question marks.

    a-name
    var3
    is-legal?
    math/exponent

The syntax uses parentheses to denote function application. Procs (and
built-ins) are can be applied with arguments by specifying the name of the proc
followed by zero or more parameters.

    (f)
    (f arg1)
    (f arg1 arg2)

Lists of parameters can also be provided, expanding
them with an ellipsis.

    (f args...)
    (f arg1 more-args...)

Line comments are written with a semicolon prefix.

    ; this is a comment

### Contracts

Procs enforce contracts on their inputs. Currently, contracts specify the proc
arity, although in the future they might specify types as well.

### Prelude

Limpet can load different sets of forms into the root environment, called
preludes. Currently, the default prelude includes

- `bool?`
- `and`
- `or`
- `not`

- `num?`
- arithmetic operators

- `str?`
- `str/cat`
- `str/split`
- `str/length`

- `vec?`
- `vec`
- `vec/take`
- `vec/drop`
- `vec/get`
- `vec/length`

- `proc?`

- `log`
- `panic`
