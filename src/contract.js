"use strict";

export function ContractError(msg) {
  this.msg = msg;
  this.name = "contract-error";
  this.stack = (new Error()).stack;
}
ContractError.prototype = new Error;


export function validateContract(procName, proc, providedArgs) {
  if (proc.varg === undefined) {
    if (proc.args.length !== providedArgs.length) {
      throw new ContractError(`${procName} called with ${providedArgs.length} parameters but expected exactly ${proc.args.length}`);
    }
  } else if (proc.args.length > providedArgs.length) {
    throw new ContractError(`${procName} called with ${providedArgs.length} parameters but expected ${proc.args.length} or more`);
  }
}
