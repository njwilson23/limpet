"use strict";

import { Bool, Num, Str, Nil } from "./types.js";
import { warnln } from "./logging.js";

// Tokenize raw input and return a tree.
// If raw is false or undefined, the code is wrapped in a do block.
// If raw is true, the code is parsed as a single standalone expression.
export function parse(input, raw) {
  let tokens = tokenize(input);
  if (raw === undefined || !raw) {
    tokens = wrapTopLevel(tokens);
  }
  return buildAst(tokens);
}

export function tokenize(input) {
  let n = input.length;
  let end = 0;
  let start = 0;
  let tokens = [];
  while (start !== n) {
    if (input[end] === " " || input[end] === "\n") { // end of a token
      if (start !== end) {
        tokens.push(input.slice(start, end));
      }
      end = end + 1; // skip space
      start = end;
    } else if (input[end] === ";") { // comment
      // complete token and skip to end of line
      if (start !== end) {
        tokens.push(input.slice(start, end));
      }
      let i = end;
      while (input[i] !== "\n" && i < n) {
        i = i + 1;
      }
      start = i;
      end = i;
    } else if (input[end] === "(") { // start form
      tokens.push("(");
      end = end + 1; // skip over
      start = end;
    } else if (input[end] === ")") { // end form
      if (start !== end) {
        tokens.push(input.slice(start, end));
      }
      tokens.push(")");
      end = end + 1; // skip over
      start = end;
    } else if (input.slice(end, end+3) === "nil") { // nil
      tokens.push("()");
      end = end + 3; // skip over
      start = end;
    } else if (input[end] === "\"") { // read a string
      let i = end + 1;
      let esc = false;
      while (input[i] !== "\"" && !esc) {
        if (esc) {
          esc = false;
        }
        if (input[i] === "\\") {
          esc = true;
        }
        i = i + 1;
      }
      end = i + 1;
      tokens.push(input.slice(start, end));
      start = end;
    } else {
      end = end + 1;
    }

    if (end === n) {
      if (start !== end) {
        tokens.push(input.slice(start));
      }
      start = n;
    }
  }
  return tokens;
}

// top-level expressions in a limpet program are implicitly wrapped in (do exprs...)
function wrapTopLevel(tokens) {
  return ["(", "do"].concat(tokens).concat([")"])
}

// return a tree represented by arrays of values. values are either
// built in types (e.g. Bool, Num, and Str) or references to bound expressions
// within some environment
function buildAst(tokens) {
  let [ast, consumed] = _buildAst(tokens);

  if (consumed !== tokens.length) {
    warnln("not all tokens were consumed, which is unexpected!");
  }
  return ast;
}

// form an AST (or sub-AST) from a stream of tokens, returning the tree and the number of tokens consumed
function _buildAst(tokens) {

  let idx = 0;
  let ast = [];
  while (idx < tokens.length) {
    const token = tokens[idx];
    idx = idx + 1;

    if (token === ")") { 
      break;
    }

    if (token === "(") {
      if (idx !== 1) { // Parse a subexpression
        // Subtract 1 because we want the subexpression parser to see the open paren
        let [subAst, consumed] = _buildAst(tokens.slice(idx-1));
        // Subtract 1 from consumed because we rewound in the line above
        idx = idx + consumed - 1;
        ast.push(subAst);
      }
    } else {
      ast.push(atom(token))
    }
  }

  return [ast, idx]
}

// Numbers may include an optional minus, a single decimal and any number of underscores (no semantic value)
const reNumber = /^-{0,1}[0-9_]+(.[0-9_]*){0,1}$/;

// Names are
// - start with a letter
// - may include '-' or '/' internally
// - may have numbers anywhere but the first character
// - may end with a '?'
// Names in function calls and function definitions may also be followed by '...'
const reName = /^[A-Za-z]{1}([A-Za-z0-9-\/]*[A-Za-z0-9]){0,1}\?{0,1}(\.{3})?$/;

// Strings are indicated by double quotes and may contain escaped double quotes (TODO)
const reString = /^".*"$/;

const reBool = /^#t|#f$/;

function atom(token) {
  if (reBool.test(token)) {
    if (token === "#t") { return new Bool(true); }
    else { return new Bool(false); }

  } else if (reNumber.test(token)) {
    let num = Number(token);
    if (Number.isNaN(num)) {
      throw new Error(`invalid number: ${token}`);
    } else {
      return new Num(num);
    }

  } else if (reString.test(token)) {
    return new Str(token.slice(1, -1));

  } else if (token === "()") {
    return Nil

  } else if (reName.test(token)) {
    return token;

  // Handle some special tokens
  } else if (["+", "-", "*", "/", "=", ">", "<", ">=", "<="].includes(token)) {
    return token;

  } else {
    throw new Error(`failed to parse atom: ${token}`);
  }
}
