"use strict";

export function Bool(kind) {
  this.kind = kind;
}

export function Num(value) {
  this.value = value;
}

export function Str(contents) {
  this.contents = contents;
}

export function Vector(items) {
  this.items = items;
}

export const Nil = new Object;

// args is a list of the bindings expr requires
// if the proc accepts varargs, then varargs if the name of the vararg list, otherwise undefined
export function Proc(args, varg, expr, env) {
  this.args = args;
  this.varg = varg;
  // AST
  this.expr = expr;
  // Enclosing environment
  this.env = env;
}

// A builtin provided by the runtime. Like a Proc, except that it has no environment and rather than
// evaluating an AST, it executes a Javascript implementation
export function Builtin(impl, args, varg) {
  this.args = args;
  this.varg = varg;
  this.impl = impl
}

export const isLimpetValue = v => v instanceof Bool ||
                                  v instanceof Num ||
                                  v instanceof Str ||
                                  v instanceof Vector ||
                                  v === Nil