"use strict";

export const debugln = (anything, ...rest) => console.log("DEBUG", anything, ...rest);
export const warnln = (anything, ...rest) => console.log("WARN", anything, ...rest);