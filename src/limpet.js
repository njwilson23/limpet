"use strict";

import { Bool, Num, Str, Proc, Vector, Nil } from "./types.js";
import { parse } from "./parser.js";
import { evaluate } from "./evaluate.js";
import { PanicError, prelude } from "./prelude.js";
import { ContractError } from "./contract.js";
import { debugln } from "./logging.js";

export { parse, evaluate, prelude };

export function show(value) {
  if (value instanceof Bool) {
    return value.kind.toString();
  } else if (value instanceof Num) {
    return value.value.toString();
  } else if (value instanceof Str) {
    return value.contents;
  } else if (value instanceof Vector) {
    let items = value.items.map(show).reduce((a, b) => `${a}, ${b}`);
    return `{${items}}`;
  } else if (value === Nil) {
    return "nil";
  } else if (value instanceof Proc) {
    let arity = value.args.length;
    let varg = (value.varg === undefined) ? "" : "+";
    return `[proc (arity ${arity}${varg})]`;
  } else if (value instanceof Array) {
    let contents = value
      .map(show)
      .reduce(
        (acc, s) => (acc.length == 0) ? s : `${acc}\n${s}`,
        ""
      );
    return `(${contents})`;
  } else if (typeof value === 'string') {
    return value;
  } else if (typeof value === 'boolean') {
    return value;
  } else if (value instanceof ContractError) {
    return `${value.name}: ${value.msg}`;
  } else if (value instanceof PanicError) {
    return `${value.name}: ${value.msg}`;
  } else if (value instanceof Error) {
    return value.toString();
  } else {
    return `[unrepresentable: ${value} ${typeof value}]`
  }
}
