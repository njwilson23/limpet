import { Bool, Num, Str, Proc, Vector } from "./types.js";
import { Env } from "./env.js";
import { parse, tokenize } from "./parser.js";
import { evaluate } from "./evaluate.js";
import { prelude } from "./prelude.js";
import { debugln } from "./logging.js";

export { testAll };

const println = (anything, ...rest) => console.log(anything, ...rest);
const printfail = message => println(`🛑 ${message}`);

function assertEqual(left, right, message) {

  const id = a => a;


  if (left instanceof Array) {
    if (!right instanceof Array) {
      printfail(`${message || ""}\n ${left} and ${right} are not of the same type (Array)`);
      return false;
    }

    if (left.length !== right.length) {
      printfail(`${message || ""}\n '${left}' and '${right}' have different lengths (${left.length} and ${right.length})`);
      return false;
    }

    let allEqual = left.map((el, i) => assertEqual(el, right[i], `${message} \n in arrays ${left} and ${right} at position ${i}`)).every(id);
    return allEqual;

  } else if (left instanceof Object) {

    if (!right instanceof Object) {
      let detail = `${left} and ${right} are not of the same type (Object)`;
      printfail(`TEST fAILURE: ${message || ""}\n  ${detail}`);
      return false;
    }

    let leftKeys = Object.keys(left).sort();
    let rightKeys = Object.keys(right).sort();
    if (!assertEqual(leftKeys, rightKeys, `${message}\n object keys were not equal`)) {
      return false;
    }

    let allEqual = leftKeys.map((el, i) => assertEqual(left[el], right[rightKeys[i]], `${message}\n in objects ${left} and ${right} at key ${el}`)).every(id);
    return allEqual;

  } else if (left !== right) {

    printfail(`${message}\n '${left}' did not equal '${right}'`);
    return false;
  }

  return true;
}

function assertThrows(errorName, func, message) {
  try {
    func()
  } catch (e) {
    if (e.name === errorName) {
      return true;
    } else {
      printfail(`threw ${e} but expected ${errorName}`)
      return false;
    }
  }
  printfail(`${message || ""}\nexpected ${errorName} but no error thrown`)
  return false;
}

const parseRaw = input => parse(input, true);

const envTests = () => {

  const scopeTest = () => {
    let env = new Env({});
    env.bind("a", 1);
    assertEqual(env.find("a"), 1, "retrieve binding from env")
  }

  const nestedScopeTest = () => {
    let env = new Env({});
    env.bind("a", 1);
    env.bind("b", 2);
    let nested = env.scope({});
    nested.bind("a", 3);
    assertEqual(env.find("a"), 1, "binding in top scope is unchanged");
    assertEqual(nested.find("b"), 2, "binding in lower scope inherits from parent");
    assertEqual(nested.find("a"), 3, "binding in lower scope overrides parent");
  }

  const shadowedScopeTest = () => {
    const program = `
  (def foo "first declaration")

  (do
    (def foo "second declaration")
    (if (not (= "second declaration" foo)) (panic "expected second declaration") nil))

  (if (not (= "first declaration" foo)) (panic "expected first declaration") nil)
  `
    evaluate(prelude())(parse(program));
  }

  scopeTest();
  nestedScopeTest();
  shadowedScopeTest();
};

const ifTests = (interpretInCleanEnv) => {
  assertEqual(parseRaw("(if #t 1 2)"),
              ["if", new Bool(true), new Num(1), new Num(2)],
              "parsing if expressions")

  assertEqual(interpretInCleanEnv(["if", new Bool(true), new Num(1), new Num(2)]),
              new Num(1),
              "evaluating if expressions; true case")

  assertEqual(interpretInCleanEnv(["if", new Bool(false), new Num(1), new Num(2)]),
              new Num(2),
              "evaluating if expressions; false case")
};

const defTests = (interpretInCleanEnv) => {
  assertEqual(tokenize("(def foo (+ a b)"),
              ["(", "def", "foo", "(", "+", "a", "b", ")"],
              "def tokenization")

  assertEqual(interpretInCleanEnv(parseRaw("(def sum (+ 1 2))")),
              new Num(3),
              "create bindings");
};

const vecTests = (interpretInCleanEnv) => {
  assertEqual(interpretInCleanEnv(parseRaw("(vec/drop 2 (vec 1 2 3))")), new Vector([new Num(3)]), "vec/drop");
}

const procTests = (interpretInCleanEnv) => {
  assertEqual(tokenize("(def (plus a b) (+ a b))"),
              ["(", "def", "(", "plus", "a", "b", ")", "(", "+", "a", "b", ")", ")"],
              "proc definition tokenization")

  assertEqual(parseRaw("(def (plus a b) (+ a b))"),
              ["def", ["plus", "a", "b"], ["+", "a", "b"]],
              "proc definition parsing")

  let interpreter = evaluate(prelude());
  let testProc = interpreter(["def", ["plus", "a", "b"], ["+", "a", "b"]]);
  assertEqual(testProc.expr,
              new Proc(["a", "b"], undefined, ["+", "a", "b"], prelude()).expr,
              "evaluate proc definition expr")

  assertEqual(testProc.args,
              new Proc(["a", "b"], undefined, ["+", "a", "b"], prelude()).args,
              "evaluate proc definition expr")

  assertEqual(interpreter(["plus", new Num(42), new Num(137)]),
              new Num(179),
              "apply previously-defined proc")

  let varArgAst = parseRaw("(def (vmap f args...) (map f args))");
  assertEqual(varArgAst, ["def", ["vmap", "f", "args..."], ["map", "f", "args"]], "AST with varargs")
  let varArgProc = interpretInCleanEnv(varArgAst);
  assertEqual(varArgProc.args, ["f"])
  assertEqual(varArgProc.varg, "args")

  interpreter = evaluate(prelude());
  let ast = parseRaw(`
  (def (count-args args...)
    (vec/length args))`);
  interpreter(ast);
  assertEqual(interpreter(parseRaw("(count-args 1 2 3 4 5)")), new Num(5), "calling a vararg proc")

  ast = parseRaw(`
  (def (str-merge prefix args...)
    (str/cat prefix (str/cat args...)))`);
  interpreter(ast);
  assertEqual(interpreter(parseRaw("(str-merge \"goodnight\" \" \" \"moon\")")), new Str("goodnight moon"), "calling a proc with args and varargs")

  let threw = false;
  try {
    interpreter(parseRaw("(vec/length undefined)"))
  } catch (e) {
    threw = true
    assertEqual(e.message, "no binding for 'undefined'")
  }
  assertEqual(threw, true, "should throw an error when parameters aren't defined")

  threw = false;
  try {
    // Attempt to apply a number as if it were a proc
    interpreter(parseRaw("(def f 1)"))
    interpreter(parseRaw("(f 2 3)"))
  } catch (e) {
    threw = true
    assertEqual(e.message, "'f' cannot be applied")
  }
  assertEqual(threw, true, "should throw an error when parameters aren't defined")


  // defs can be used inline, since they return the proc they define
  interpreter = evaluate(prelude());
  assertEqual(interpreter(parseRaw("((def (add a b) (+ a b)) 1 2)")), new Num(3), "should apply inline proc defs");
  assertEqual(interpreter(parseRaw("(+ (def a 1) (def b 2))")), new Num(3), "should apply inline scalar defs");


  interpreter = evaluate(prelude());
  interpreter(parseRaw(`(def (one-arg arg1) #t)`));
  interpreter(parseRaw(`(def (two-plus-args arg1 arg2 rest...) #t)`));
  assertThrows(
    "contract-error",
    () => interpreter(parseRaw("(one-arg 1 2)"))
  );
  assertThrows(
    "contract-error",
    () => interpreter(parseRaw("(one-arg)"))
  );
  assertEqual(
    interpreter(parseRaw("(one-arg 1)")), new Bool(true), "should allow correct argument count"
  );
  assertThrows(
    "contract-error",
    () => interpreter(parseRaw("(two-plus-args 1)"))
  );
  assertEqual(
    interpreter(parseRaw("(two-plus-args 1 2)")), new Bool(true), "should allow no vargs"
  );
  assertEqual(
    interpreter(parseRaw("(two-plus-args 1 2 3 4)")), new Bool(true), "should allow vargs"
  );
};

const integrationTests = () => {

  const run = program => {
    let interpreter = evaluate(prelude());
    return interpreter(parse(program));
  }

  let fibProgram = `
(def (fib n)
  (if (or (= n 0) (= n 1))
    n
    (+ (fib (- n 1)) (fib (- n 2)))))

(fib 15)`

  assertEqual(run(fibProgram), new Num(610), "fib(15)");


  let recurProgram = `
(def (recur n)
  (if (= n 0)
      1
      (recur (- n 1))))

(recur 500000)`

  assertEqual(run(recurProgram), new Num(1), "tail recursive")

  let lengthProgram =`
(def (length as)
  (if (vec/empty? as)
    0
    (+ 1 (length (vec/drop 1 as)))))

(vec
  (length (vec))
  (length (vec 1))
  (length (vec 1 2))
  (length (vec 1 2 3)))
`

  let lengthResult = run(lengthProgram);
  assertEqual(lengthResult.items[0], new Num(0), "recursive vec length (0)")
  assertEqual(lengthResult.items[1], new Num(1), "recursive vec length (1)")
  assertEqual(lengthResult.items[2], new Num(2), "recursive vec length (2)")
  assertEqual(lengthResult.items[3], new Num(3), "recursive vec length (3)")
};

const doTests = () => {
  let doAssignmentProgram = `
(def data
     (do
        1
        2
        3))

data`;
  assertEqual(evaluate(prelude())(parse(doAssignmentProgram)), new Num(3), "do-forms should return the last expression");
};

const parserTests = () => {
  assertEqual(tokenize("name 3 true"), ["name", "3", "true"], "value tokenization")
  assertEqual(tokenize("name \"hello world\" true"), ["name", "\"hello world\"", "true"], "string tokenization")
  assertEqual(tokenize("1\n\n"), ["1"], "tokenization ignores newlines")
  assertEqual(tokenize("(a b rest...)"), ["(", "a", "b", "rest...", ")"], "varargs tokenization")

  assertEqual(tokenize("(+ (+ 1 3) (- 10 (+ 2 1)))"),
              ["(", "+", "(", "+", "1", "3", ")", "(", "-", "10", "(", "+", "2", "1", ")", ")", ")"],
              "tokenizing nested expressions")

  assertEqual(tokenize("(+ 1 1) ; same line comment"),
              ["(", "+", "1", "1", ")"],
              "tokenizing with same line comments")

  assertEqual(tokenize("(+ 1 1);same line comment"),
              ["(", "+", "1", "1", ")"],
              "tokenizing with same line comments and no spaces")

  assertEqual(tokenize("(+ 1 1)\n; own line comment\n(- 5 3)"),
              ["(", "+", "1", "1", ")", "(", "-", "5", "3", ")"],
              "tokenizing own line comments")

  assertEqual(parseRaw("(+ (+ 1 3) (- 10 (+ 2 1)))"),
              ["+", ["+", new Num(1), new Num(3)], ["-", new Num(10), ["+", new Num(2), new Num(1)]]],
              "parsing nested expressions")

  assertEqual(parseRaw("#t")[0],
              new Bool(true),
              "boolean parsing");

  assertEqual(parseRaw("\"hello world\""),
              [new Str("hello world")],
              "string parsing");

  assertEqual(parseRaw("((def (add a b) (+ a b)) 1 2)"),
             [["def", ["add", "a", "b"], ["+", "a", "b"]], new Num(1), new Num(2)],
             "parse inline proc definitions");

  assertEqual(parse("((def (add a b) (+ a b)) 1 2)"),
             ["do", [["def", ["add", "a", "b"], ["+", "a", "b"]], new Num(1), new Num(2)]],
             "parse inline proc definitions (do-form)");
};

const testAll = () => {
  let interpretInCleanEnv = (expr) => evaluate(prelude())(expr);

  assertEqual(interpretInCleanEnv(["and", new Bool(true), new Bool(false)]),
              new Bool(false),
              "and function evaluation");

  assertEqual(interpretInCleanEnv(["or", new Bool(true), new Bool(false)]),
              new Bool(true),
              "or function evaluation");

  assertEqual(interpretInCleanEnv(["and", ["or", new Bool(true), new Bool(false)], new Bool(true)]),
              new Bool(true),
              "compound boolean evaluation");

  assertEqual(interpretInCleanEnv(["not", new Bool(true)]),
              new Bool(false),
              "not function evaluation");

  assertEqual(interpretInCleanEnv(["not", new Bool(false)]),
              new Bool(true),
              "not function evaluation (2)");

  assertEqual(interpretInCleanEnv(["str/cat", new Str("hello"), new Str(" "), new Str("world")]),
              new Str("hello world"),
              "str/cat evaluation");

  assertEqual(interpretInCleanEnv(["str/length", new Str("hello world")]),
              new Num(11),
              "str/length evaluation");

  assertEqual(interpretInCleanEnv(["str/split", new Str(" "), new Str("hello world and goodbye")]),
              new Vector([new Str("hello"), new Str("world"), new Str("and"), new Str("goodbye")]),
              "str/split evaluation");

  assertEqual(interpretInCleanEnv(parseRaw("(- (+ 1 (* 2 3)) 5)")),
              new Num(2),
              "program: simple arithmetic");

  assertEqual(interpretInCleanEnv(parseRaw("(vec/get 0 (vec 1))")),
              new Num(1),
              "program: vector indexing");

  assertEqual(interpretInCleanEnv(parseRaw("(vec/get 2 (vec 1 2 3))")),
              new Num(3),
              "program: vector indexing (2)");

  // Type tests

  assertEqual(interpretInCleanEnv(parseRaw("(num? 1)")), new Bool(true), "1 is a number")
  assertEqual(interpretInCleanEnv(parseRaw("(num? #t)")), new Bool(false), "true is not a number")

  assertEqual(interpretInCleanEnv(parseRaw("(str? \"cat\")")), new Bool(true), "\"cat\" is a string")
  assertEqual(interpretInCleanEnv(parseRaw("(str? #f)")), new Bool(false), "false is not a number")

  assertEqual(interpretInCleanEnv(parseRaw("(bool? #f)")), new Bool(true), "false is a boolean")
  assertEqual(interpretInCleanEnv(parseRaw("(bool? 0)")), new Bool(false), "zero is not a boolean")

  assertEqual(interpretInCleanEnv(parseRaw("(vec? (vec #t #f))")), new Bool(true), "vec constructs a vector")
  assertEqual(interpretInCleanEnv(parseRaw("(vec? nil)")), new Bool(true), "nil is a vector")
  assertEqual(interpretInCleanEnv(parseRaw("(vec? -1)")), new Bool(false), "a number is not a vector")

  // Equality tests

  assertEqual(interpretInCleanEnv(parseRaw("(= 1 1)")),
              new Bool(true),
              "equality for numbers")

  assertEqual(interpretInCleanEnv(parseRaw("(= #t #t)")),
              new Bool(true),
              "equality for bools")

  assertEqual(interpretInCleanEnv(parseRaw("(= \"abc\" \"abc\")")),
              new Bool(true),
              "equality for strings")

  assertEqual(interpretInCleanEnv(parseRaw("(= nil nil)")),
              new Bool(true),
              "equality for nil")

  assertEqual(interpretInCleanEnv(parseRaw("(= 1 -1)")),
              new Bool(false),
              "inequality for numbers")

  assertEqual(interpretInCleanEnv(parseRaw("(= #t #f)")),
              new Bool(false),
              "inequality for bools")

  assertEqual(interpretInCleanEnv(parseRaw("(= \"abc\" \"def\")")),
              new Bool(false),
              "inequality for strings")

  assertEqual(interpretInCleanEnv(parseRaw("(= nil 1)")),
              new Bool(false),
              "inequality for nil")

  assertEqual(interpretInCleanEnv(parseRaw("(= 1 nil)")),
              new Bool(false),
              "inequality for nil")

  envTests();

  parserTests();

  vecTests(interpretInCleanEnv);

  ifTests(interpretInCleanEnv);

  defTests(interpretInCleanEnv);

  procTests(interpretInCleanEnv);

  doTests();

  integrationTests();
}
