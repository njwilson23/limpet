"use strict";

import { Bool, Proc, Vector, Nil, isLimpetValue, Builtin } from "./types.js";
import { validateContract } from "./contract.js";
import { debugln } from "./logging.js";

// Curried version of evaluate loop for ease of passing an associated environment
export function evaluate(env) {
  return function(expr) {
    return evaluate_loop(env, expr)
  }
}

// Evaluate takes an expression (a value, a name, or an AST) and returns the
// evaluated value.
//
// This implementation of evaluate uses a loop for tail calls, so that limpet
// recursion is stack-safe.
//
// This works based on the property that all `def` forms contain a single RHS
// expression that is necessarily in tail position and can take ownership of the
// (Proc) caller's environment.  Other forms (like 'if' and 'do') and function
// application are _not_ tail recursive, and do not re-use the caller's
// environment.
function evaluate_loop(env, expr) {

  let currentEnv = env;
  let currentExpr = expr;
  // When tailCall is true, proc evaluation will re-use the current environment rather than creating a new frame.
  let tailCall = false;

  while (true) {
    if (currentExpr instanceof Array) {
      if (currentExpr.length === 0) {
        return null;
      }

      let [op, ...args] = currentExpr;
      if (op === "def") { // def-form
        if (args.length !== 2) {
          throw new Error("'def' accepts either a name followed by a single expression, or a name and list of parameters followed by a single expression");
        }

        if (args[0] instanceof Array) { // This is defining a proc
          let sig = args[0];
          let currentExpr = args[1];

          if (sig.length === 0) {
            throw new Error("proc must be named")
          }
          let name = sig[0];
          let procArgs = sig.slice(1);
          let proc;
          if (procArgs.length !== 0 && procArgs[procArgs.length-1].endsWith("...")) {
            let varg = procArgs[procArgs.length-1].slice(0, -3);
            proc = new Proc(procArgs.slice(0, -1), varg, currentExpr, currentEnv)
          } else {
            proc = new Proc(procArgs, undefined, currentExpr, currentEnv)
          }
          currentEnv.bind(name, proc)
          return proc;
        } else {
          // This is creating a constant binding
          let name = args[0];
          let currentExpr = args[1];
          let value = evaluate_loop(currentEnv, currentExpr);
          currentEnv.bind(name, value);

          // Defs return the RHS as a value, in addition to altering the environment
          return value;
        }
      } else if (op === "do") { // do-form
        let scope = currentEnv;
        for (expr of args.slice(0, -1)) {
          scope = scope.scope({});
          evaluate_loop(scope, expr);
        }
        if (args.length !== 0) {
          currentEnv = scope;
          currentExpr = args[args.length - 1];
          tailCall = true;
        } else {
          // TODO: do we really want to use an empty list as null?
          return Nil;
        }
      } else if (op === "if") { // if-form
        if (args.length !== 3) {
          throw new Error("'if' forms require a predicate, a true branch, and a false branch")
        }
        let pred = args[0];
        let whenTrue = args[1];
        let whenFalse = args[2];

        let p = evaluate_loop(currentEnv, pred);
        if (!p instanceof Bool) {
          throw new Error("'if' forms must have a boolean predicate");
        }

        // Set the correct branch to the currentExpr evaluate on the next loop.
        currentExpr = (p.kind) ? whenTrue : whenFalse;
        tailCall = true;
      } else {
        // Associate with an existing binding that can be applied
        let appliable;
        if (typeof op === "string") {
          appliable = currentEnv.find(op);
          if (appliable === null) {
            throw new Error(`not an implemented proc: ${op}`);
          }
        } else {
          appliable = evaluate_loop(currentEnv, op);
        }

        if (appliable instanceof Proc) { // Apply a proc
          validateContract(op, appliable, args)
          let providedArgs = expandArgs(currentEnv, args);
          // Evaluate each argument and add them to the Proc's closure
          let applyEnv = appliable.args.reduce(
            (newEnv, argName, i) => {
              newEnv.bindOrReplace(argName, providedArgs[i]);
              return newEnv;
            },
            // If this is a tail call, we can write parameters directly into the current environment.
            // Otherwise, we need to create a new scope to prevent them from leaking.
            tailCall ? appliable.env : appliable.env.scope({})
          );
          if (appliable.varg !== undefined) {
            // The proc takes varargs, so add a vector containing all remaining parameters to the closure
            applyEnv.bindOrReplace(appliable.varg, new Vector(providedArgs.slice(appliable.args.length)));
          }

          // Set the new environment for the Proc and the Proc's expression to
          // currentEnv/currentExpr so that they're evaluated on the next loop.
          currentEnv = applyEnv;
          currentExpr = appliable.expr;
        } else if (appliable instanceof Builtin) { // Apply a built-in
          validateContract(op, appliable, args)
          let providedArgs = expandArgs(currentEnv, args);
          return appliable.impl(providedArgs);
        } else {
          throw new Error(`'${op}' cannot be applied`)
        }
      }
    } else if (isLimpetValue(currentExpr)) {
      return currentExpr;
    } else if (typeof currentExpr === 'string' && currentEnv.find(currentExpr) !== null) {
      // currentExpr is a name
      return currentEnv.find(currentExpr);
    } else {
      throw new Error(`no binding for '${currentExpr}'`)
    }
  }
}

// Given a list of arguments and an environment, expand the arguments into a list
// of expressions. The means evaluating each argument, and handling splatted arguments.
function expandArgs(env, args) {
  let expanded = [];

  for (const arg of args) {
    if (typeof arg === 'string' && arg.endsWith("...")) {
      let splatted = env.find(arg.slice(0, -3));
      if (splatted === null || !splatted instanceof Vector) {
        throw new Error(`proc parameters expended with '...' must be bound vectors ${arg}`)
      }
      if (splatted !== null) {
        for (const splattedArg of splatted.items) {
          expanded.push(evaluate_loop(env, splattedArg));
        }
      } else {
        throw new Error(`no binding for ${arg.slice(0, -3)}`);
      }
    } else {
      expanded.push(evaluate_loop(env, arg));
    }
  }

  return expanded;
}