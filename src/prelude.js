"use strict";

import { Bool, Num, Str, Proc, Builtin, Vector, Nil } from "./types.js";
import { Env } from "./env.js";
import { parse } from "./parser.js";
import { evaluate } from "./evaluate.js";
import { debugln } from "./logging.js";

// Return an environment populated with basic procs that can be used as a root environment
const builtins = () => new Env({
  // logic
  "and": new Builtin(args => args.reduce((a, b) => new Bool(a.kind && b.kind)), ["first"], "rest"),
  "or": new Builtin(args => args.reduce((a, b) => new Bool(a.kind || b.kind)), ["first"], "rest"),
  "not": new Builtin(([arg]) => new Bool(!arg.kind), ["term"]),

  // arithmetic
  "+": new Builtin(args => args.reduce((a, b) => new Num(a.value + b.value)), ["first"], "rest"),
  "*": new Builtin(args => args.reduce((a, b) => new Num(a.value * b.value)), ["first"], "rest"),
  "-": new Builtin(args => args.reduce((a, b) => new Num(a.value - b.value)), ["first"], "rest"),
  "/": new Builtin(args => args.reduce((a, b) => new Num(a.value / b.value)), ["first"], "rest"),
  "<": new Builtin(([l, r]) => new Bool(l.value < r.value), ["left", "right"]),
  ">": new Builtin(([l, r]) => new Bool(l.value > r.value), ["left", "right"]),
  "<=": new Builtin(([l, r]) => new Bool(l.value <= r.value), ["left", "right"]),
  ">=": new Builtin(([l, r]) => new Bool(l.value >= r.value), ["left", "right"]),
  "=": new Builtin(([left, right]) => {
    if (left === Nil) {
      return new Bool(right === Nil)
    }

    if (right === Nil) {
      return new Bool(left === Nil)
    }

    // this doesn't need to do a deep traversal as long as all user-facing types are shallow
    let keys = new Set(Object.keys(left));
    for (let key of Object.keys(right)) {
      keys.add(key);
    }
    for (let key of keys) {
      if (!left.hasOwnProperty(key) || !right.hasOwnProperty(key) || left[key] !== right[key]) {
        return new Bool(false);
      }
    }
    return new Bool(true);
  }, ["left", "right"]),
  "pow": new Builtin(([base, exp]) => new Num(Math.pow(base.value, exp.value)), ["base", "exponent"]),

  // type tests
  "str?": new Builtin(([arg]) => new Bool(arg instanceof Str), ["test"]),
  "num?": new Builtin(([arg]) => new Bool(arg instanceof Num), ["test"]),
  "bool?": new Builtin(([arg]) => new Bool(arg instanceof Bool), ["test"]),
  "proc?": new Builtin(([arg]) => new Bool(arg instanceof Proc), ["test"]),
  "vec?": new Builtin(([arg]) => new Bool(arg instanceof Vector || arg === Nil), ["test"]),

  // strings
  "str/cat": new Builtin(args => args.reduce((a, b) => new Str(a.contents.concat(b.contents))), ["first"], "rest"),
  "str/length": new Builtin(([s]) => new Num(s.contents.length), ["string"]),
  "str/split": new Builtin(([delim, s]) => new Vector(s.contents.split(delim.contents).map(part => new Str(part))), ["delimiter", "string"]),

  // vectors
  "vec/empty?": new Builtin(([vec]) => new Bool(vec.items.length === 0), ["vector"]),
  "vec/take": new Builtin(([n, vec]) => new Vector(vec.items.slice(0, n.value)), ["n", "vector"]),
  "vec/drop": new Builtin(([n, vec]) => new Vector(vec.items.slice(n.value)), ["n", "vector"]),
  "vec/get": new Builtin(([i, vec]) => vec.items[i.value], ["i", "vector"]),

  // utilities
  "date": new Builtin(([]) => {
    let d = new Date
    return new Vector([
      new Num(d.getUTCFullYear()),
      new Num(d.getUTCMonth() + 1),
      new Num(d.getUTCDate())
    ])
  }, []),
  "time": new Builtin(([]) => {
    let d = new Date
    return new Vector([
      new Num(d.getUTCHours()),
      new Num(d.getUTCMinutes()),
      new Num(d.getUTCSeconds()),
      new Num(d.getUTCMilliseconds())
    ])
  }, []),
  "log": new Builtin(([a]) => {
    console.log("LOG", a);
    return a;
  }, ["value"]),
  "panic": new Builtin(([msg]) => {
    throw new PanicError(msg.contents);
  }, ["message"]),
});

export function PanicError(msg) {
  this.msg = msg;
  this.name = "panic-error";
  this.stack = (new Error()).stack;
}
PanicError.prototype = new Error;


// Return an environment populated with basic procs that can be used as a root environment
export const prelude = () => {
  let env = builtins();

  evaluate(env)(parse("(def (vec vs...) vs)", true));

  evaluate(env)(parse(
`(def (vec/length v)
  (if (vec/empty? v)
    0
    (+ 1 (vec/length (vec/drop 1 v)))))`,
    true
  ));

  return env.scope({});
};
