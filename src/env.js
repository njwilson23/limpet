"use strict";

export function Env(bindings) {
  this.bindings = bindings;
  this.up = null;
}

const envPrototype = {
  scope(bindings) {
    let newEnv = new Env(bindings);
    newEnv.up = this;
    return newEnv;
  },

  find(name) {
    if (Object.hasOwn(this.bindings, name)) {
      return this.bindings[name];
    } else if (this.up !== null) {
      return this.up.find(name);
    } else {
      return null;
    }
  },

  bind(name, expr) {
    if (this.bindings.hasOwnProperty(name)) {
      throw Error(`cannot redefine '${name}'`);
    }
    this.bindOrReplace(name, expr);
  },

  // Like bind, but rather than throwing if a binding exists, replace it. This is used for optimizing tail calls.
  bindOrReplace(name, expr) {
    this.bindings[name] = expr;
  },
}

Object.assign(Env.prototype, envPrototype);
